module IndexHelper

	def entries(total, objects)
		unless total.zero?
			content_tag :div, class: "entries hidden-xs" do
				if objects.first_page?
					"1 al #{objects.size} de #{total} registros"
				elsif objects.last_page?
					"#{((objects.current_page * objects.default_per_page) - objects.default_per_page)+1} al #{total} de #{total} registros"
				else
					"#{((objects.current_page * objects.default_per_page) - objects.default_per_page)+1} al #{objects.current_page * objects.default_per_page} de #{total} registros"
				end
			end
		end
	end

	def show_blog?
		controller_name.eql? "blog" and action_name.eql? "show"
	end

	def listing_blog?
		controller_name.eql? "blog" and action_name.eql? "index"
	end

end