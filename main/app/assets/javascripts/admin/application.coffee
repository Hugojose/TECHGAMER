#= require jquery
#= require jquery.turbolinks
#= require jquery_ujs
#= require angular
#= require sidebar
#= require ckeditor/init
#= require cocoon
#= require keppler_blog/admin/application
#= require noty
#= require checklist-model
#= require angular-local-storage
#= require materialize-sprockets
#= require nprogress
#= require nprogress-turbolinks
#= require ckeditor/init
#= require turbolinks
#= require_tree .
		
