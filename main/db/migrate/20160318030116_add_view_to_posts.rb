class AddViewToPosts < ActiveRecord::Migration
  def change
    add_column :keppler_blog_posts, :views, :integer, :default => 0
  end
end
