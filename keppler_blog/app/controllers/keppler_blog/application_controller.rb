module KepplerBlog
  class ApplicationController < ::ApplicationController
  	protect_from_forgery except: :index
  end
end
