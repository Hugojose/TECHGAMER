
#Generado con Keppler.
require_dependency "keppler_blog/application_controller"

module KepplerBlog
  class BlogController < ApplicationController 
    protect_from_forgery except: :index
    impressionist :actions=>[:show]

    layout 'frontend/application'
    before_action :set_data_widgets, only: [:index, :show, :filter, :filter_subcategory]

    def index
      @categories = Category.all.order(:name)

     # @posts = Post.searching(params[:query]).where(public: true).page(@current_page).per(KepplerBlog.posts_per_page)
      @posts = KepplerBlog::Category.where.not(permalink: "ultimas-noticias").map { |c| KepplerBlog::Post.where(category_id: c.id, public: true).last }

      #Delete elementos vacios
      @posts.delete([]) 
      
      #Posts mas vistos
      @posts_more_view = KepplerBlog::Post.where(public:true).order('views DESC').limit(3)

      #Post software
      @category_software = KepplerBlog::Category.find_by_permalink("software")
      @posts_software = KepplerBlog::Post.where(public:true, category_id: @category_software.id).order("created_at DESC").page(@current_page).per(6)


      #Post Hardware
      @category_hardware = KepplerBlog::Category.find_by_permalink("hardware")
      @posts_hardware = KepplerBlog::Post.where(public:true, category_id: @category_hardware.id).order("created_at DESC").page(@current_page2).per(6)
      
      # Ultimas noticias
      @category_news = KepplerBlog::Category.find_by_permalink("ultimas-noticias")
      @posts_breaking_news = KepplerBlog::Post.where(public:true, category_id: @category_news.id).order("created_at DESC").page(@current_page3).per(6)

      #POST GUIAS
      @category_guias = KepplerBlog::Category.find_by_permalink("guias")
      
      @posts_guias = KepplerBlog::Post.where(category_id: @category_guias.id, public:true).first(3)
      respond_to do |format|
        format.html
        format.js   
      end
    end

    def query
      @posts = Post.searching(params[:query]).where(public: true).page(@current_page).per(8)
      @categories = Category.all.order(:name)
    end

    def show   
      @post =  Post.find_by_permalink(params[:permalink])
      impressionist(@post) 
    end

    def filter
      @posts = Post.send("filter_by_#{params[:type]}", params[:permalink]).page(@current_page).per(8)
      render action: 'query'
    end

    def filter_subcategory
      @posts = Post.filter_by_subcategory(params[:category], params[:subcategory]).page(@current_page).per(8)
      render action: 'query'
    end

    private

    def set_data_widgets
      @posts_recents = Post.where(public: true).order("created_at DESC").first(6)
      @categories = Category.all.order(:name)
    end

  end
end
